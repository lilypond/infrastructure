#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import json
import sys
import requests
import urllib.parse

GITLAB = 'https://gitlab.com/'

STATES = [ 'Push', 'Countdown', 'Review', 'New', 'Waiting' ]

parser = argparse.ArgumentParser(
    description='Countdown patches for LilyPond',
)
parser.add_argument(
    '--repo',
    help='Repository at ' + GITLAB,
    default='lilypond/lilypond'
)
args = parser.parse_args(sys.argv[1:])

def get_urls(repo):
    quoted = urllib.parse.quote_plus(repo)

    mr_list = GITLAB + repo + '/-/merge_requests?sort=label_priority'
    api = GITLAB + 'api/v4/projects/' + quoted + '/merge_requests'
    return mr_list, api

# Group merge requests by their state.
def group_mrs_by_state(merge_requests):
    grouped_mrs = { s: [] for s in STATES }
    unknown_mrs = []
    for mr in merge_requests:
        for state in STATES:
            if 'Patch::' + state.lower() in mr['labels']:
                grouped_mrs[state].append(mr)
                break
        else:
            for label in mr['labels']:
                if label.startswith('Patch::'):
                    break
            else:
                unknown_mrs.append(mr)
    return grouped_mrs, unknown_mrs

def print_state(merge_requests, state):
    print('     {state}:'.format(state=state))
    print('')
    if len(merge_requests) == 0:
        print('No patches in {state} at this time.'.format(state=state))
        print('')
    else:
        for mr in merge_requests:
            print('!{iid} {title} - {author}'.format(
                iid=mr['iid'],
                title=mr['title'],
                author=mr['author']['name'],
            ))
            print(mr['web_url'])
            print('')

    print('')

mr_list, api = get_urls(args.repo)

# Create a session to speed up multiple requests.
s = requests.Session()

# Load all reponses until there is no 'next' link.
merge_requests = []
page_url = api + '?state=opened&per_page=100'

while page_url:
    response = s.get(page_url)
    merge_requests += response.json()

    # Determine 'next' link.
    if not 'next' in response.links:
        break
    page_url = response.links['next']['url']

grouped_mrs, unknown_mrs = group_mrs_by_state(merge_requests)

print('A list of all merge requests can be found here:')
print(mr_list)
print('\n')

for state in STATES:
    print_state(grouped_mrs[state], state)

if len(unknown_mrs) > 0:
    print_state(unknown_mrs, 'Unknown')
