#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import re
import requests
import sys
from urllib3.util.retry import Retry

GITLAB_API = 'https://gitlab.com/api/v4'
GITLAB_API_LABELS = GITLAB_API + '/projects/{repo}/labels?per_page=100'
GITLAB_API_MILESTONE = GITLAB_API + '/projects/{repo}/milestones'

parser = argparse.ArgumentParser(
    description='Create milestones for LilyPond',
)
parser.add_argument(
    '--token',
    help=(
        'GitLab token, get it at ' +
        'https://gitlab.com/profile/personal_access_tokens'
    ),
    required=True,
)
parser.add_argument(
    '--repo',
    help='GitLab repository to process',
    required=True
)
parser.add_argument(
    '--dry-run',
    help='Only print milestones that would be created',
    action='store_true',
)

args = parser.parse_args(sys.argv[1:])
api_repo = args.repo.replace('/', '%2F')

api = requests.Session()
# Supply token
api.headers = {'Private-Token': args.token}
# Retry on "Internal Server Error"
retries = Retry(
    method_whitelist=['POST','PUT'],
    status_forcelist=[500],
)
adapter = requests.adapters.HTTPAdapter(max_retries=retries)
api.mount(GITLAB_API, adapter)

# Query all labels.
def query_labels():
    labels = []
    page_url = GITLAB_API_LABELS.format(repo=api_repo)
    while page_url:
        r = api.get(page_url)
        labels += r.json()
        if 'next' not in r.links:
            break
        page_url = r.links['next']['url']
    return labels

# Extract versions from label names.
def extract_versions(labels):
    versions = set()
    exp = re.compile('Fixed_(\d+)_(\d+)_(\d+)')
    for l in labels:
        m = exp.search(l['name'])
        if m:
            major = int(m.group(1))
            minor = int(m.group(2))
            patch = int(m.group(3))
            versions.add((major, minor, patch))
    versions = sorted(versions)
    return versions

labels = query_labels()
versions = extract_versions(labels)
milestones = [ '%d.%d.%d' % v for v in versions ]

if args.dry_run:
    print('Would create the following milestones:')
    for m in milestones:
        print('  ' + m)
    sys.exit(0)

create_milestone_url = GITLAB_API_MILESTONE.format(repo=api_repo)
for m in milestones:
    print('Creating milestone ' + m)
    api.post(create_milestone_url, { 'title': m })
