#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import collections
import re
import requests
import sys
from urllib3.util.retry import Retry

GITLAB_API = 'https://gitlab.com/api/v4'
GITLAB_API_ISSUES = GITLAB_API + '/projects/{repo}/issues?per_page=100&sort=asc'
GITLAB_API_MILESTONES = GITLAB_API + '/projects/{repo}/milestones?per_page=100'
GITLAB_API_ISSUE_EDIT = GITLAB_API + '/projects/{repo}/issues/{issue}'

parser = argparse.ArgumentParser(
    description='Assign milestones for LilyPond',
)
parser.add_argument(
    '--token',
    help=(
        'GitLab token, get it at ' +
        'https://gitlab.com/profile/personal_access_tokens'
    ),
    required=True,
)
parser.add_argument(
    '--repo',
    help='GitLab repository to process',
    required=True
)
parser.add_argument(
    '--dry-run',
    help='Only print issues that would be assigned to a milestone',
    action='store_true',
)

args = parser.parse_args(sys.argv[1:])
api_repo = args.repo.replace('/', '%2F')

api = requests.Session()
# Supply token
api.headers = {'Private-Token': args.token}
# Retry on "Internal Server Error"
retries = Retry(
    method_whitelist=['POST','PUT'],
    status_forcelist=[500],
)
adapter = requests.adapters.HTTPAdapter(max_retries=retries)
api.mount(GITLAB_API, adapter)

# Query all issues.
def query_items(url):
    items = []
    page_url = url 
    while page_url:
        r = api.get(page_url)
        items += r.json()
        if 'next' not in r.links:
            break
        page_url = r.links['next']['url']
    return items

# Determine milestones to assign.
def determine_milestones(issues, milestones):
    # For each issue, check if there is exactly one label that corresponds
    # to a version. If so, mark it to be assigned later on.
    assign = collections.OrderedDict()
    exp = re.compile('Fixed_(\d+)_(\d+)_(\d+)')
    for issue in issues:
        version = None
        for label in issue['labels']:
            m = exp.search(label)
            if not m:
                continue
            if version is not None:
                # Issue has at least two labels.
                print('https://gitlab.com/{repo}/-/issues/{issue} has two labels'.format(
                    repo=args.repo,
                    issue=issue['iid'])
                )
                version = None
                break
                
            major = int(m.group(1))
            minor = int(m.group(2))
            patch = int(m.group(3))
            version = (major, minor, patch)

        if version is not None:
            # Issue has exactly one label, determine milestone iid.
            milestone_title = '%d.%d.%d' % version

            assign[issue['iid']] = milestone_title
            continue

    return assign

issues = query_items(GITLAB_API_ISSUES.format(repo=api_repo))
milestones = query_items(GITLAB_API_MILESTONES.format(repo=api_repo))

assign = determine_milestones(issues, milestones)

if args.dry_run:
    print('Would assign the following milestones:')
    for issue, milestone in assign.items():
        print('  {:>4} => {:>7}'.format(issue, milestone))
    sys.exit(0)

# Create a dict from version to milestone id (global, not iid).
milestone_ids = {}
for m in milestones:
    milestone_ids[m['title']] = m['id']

assign_ids = collections.OrderedDict()
for issue, milestone in assign.items():
    milestone_id = milestone_ids.get(milestone, None)
    if milestone_id is None:
        print('Milestone {milestone} could not be found for #{issue}'.format(
            milestone=milestone,
            issue=issue,
        ))
        continue

    assign_ids[issue] = milestone_id

for issue_iid, milestone_id in assign_ids.items():
    print('Assigning issue #{issue} to milestone {milestone}'.format(
        issue=issue_iid,
        milestone=assign[issue_iid],
    ))

    edit_issue_url = GITLAB_API_ISSUE_EDIT.format(
        repo=api_repo,
        issue=issue_iid,
    )
    api.put(edit_issue_url, { 'milestone_id': milestone_id })
